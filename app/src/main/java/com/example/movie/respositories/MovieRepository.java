package com.example.movie.respositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.movie.models.MovieModel;
import com.example.movie.request.MovieApiClient;

import java.util.List;

public class MovieRepository {

    private static MovieRepository instance;

    private MovieApiClient movieApiClient;


    public static MovieRepository getInstance(){
        if(instance == null){
            instance =  new MovieRepository();
        }

        return instance;
    }

    private MovieRepository(){
         movieApiClient = MovieApiClient.getInstance();
    }

    public LiveData<List<MovieModel>> getMovies(){
        return movieApiClient.getMovies();
    }

    public void searchMovieApi(String query, int pageNumber){
        Log.v("TAG", "Movie Repo");
        movieApiClient.searchMoviesApi(query, pageNumber);
    }
}
